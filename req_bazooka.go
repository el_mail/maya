package maya

import (
	"net/http"
	"net/url"
)

//ReqRocket gag
type ReqRocket struct {
	Method      string
	BaseURL     string
	Endpoint    string
	QueryParams map[string]string
	Headers     map[string][]string
}

const defaultUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36"

//Bazooka gag
func Bazooka(r ReqRocket) (*http.Request, error) {

	u, err := url.Parse(r.BaseURL)
	if err != nil {
		return nil, err
	}

	if r.Endpoint != "" {
		u.Path = r.Endpoint
	}

	q := url.Values{}
	for p, v := range r.QueryParams {
		q.Set(p, v)
	}
	u.RawQuery = q.Encode()

	req, err := http.NewRequest(r.Method, u.String(), nil)
	if err != nil {
		return nil, err
	}

	if r.Headers != nil {
		req.Header = http.Header(r.Headers)
	}

	if req.Header["User-Agent"] == nil {
		req.Header.Set("User-Agent", defaultUserAgent)
	}

	return req, nil
}

//Turret gag
func Fire(c *http.Client, r ReqRocket) (*http.Response, error) {

	req, _ := Bazooka(r)

	res, err := c.Do(req)

	if err != nil {
		return nil, err
	}

	return res, nil
}
