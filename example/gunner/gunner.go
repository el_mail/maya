package main

import (
	"log"
	"runtime"

	"gitlab.com/qwzar/maya"
)

func main() {

	go proxy()

	cfg := maya.Config{}
	if err := maya.DaemonRun(&cfg); err != nil {
		log.Printf("Error in main(): %v", err)
	}
}

func proxy() {
	h := maya.NewHoneycomb(0, 0)
	numCPUs := runtime.NumCPU()
	_ = numCPUs

	//go h.BuffStatus()

	h.BeeSpawn(1, func(id int, task interface{}, h *maya.Honeycomb) {
		maya.CheckProxy(task, h.Result)
	})

	go func(name string) {
		for result := range h.Result {
			log.Println("result", name, result)
		}
	}("maya")

	go func() {
		maya.GetProxyList("Russian Federation", h.Task)
	}()
}
