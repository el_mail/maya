package maya

import (
	"log"
	"testing"
)

func TestWorker(t *testing.T) {
	task := func() error {
		log.Println("worker")
		return nil
	}
	w := Worker{1, 10, "runing", task}
	w.Run()
}
